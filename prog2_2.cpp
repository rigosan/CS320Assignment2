#include "prog2_1.hpp"
#include <string>
#include <iostream>
#include <vector>

#include <fstream>
int main(int argc,char** argv){
    printf("Assignment #2-2, Sergio Santana, rigosanle@gmail.com\n");
    Tokenizer tok;
    std::ifstream file(argv[1]);
    int j=1;
    
    //print Error if Tokenization error, thrown by Tokenize()
    try{
    while(!file.eof()){
        std::string s;
        getline(file,s);    
        tok.Tokenize(s);   
        j++;
           }
        }   
    catch(std::string e){
    std::cout<<"Error on line "<<j<<": "<< e<< std::endl;
    return 0;
    }
    
    //print out all tokens line by line
    bool last=false;
    int size=tok.getSize();
    for(int m=0;m<size;m++){
        
        std::vector<std::string> vec=tok.GetTokens();
        int vecSize=vec.size();
        for(int i=0;i<vecSize;i++){
            if(i==vecSize-1){ 
                std::cout<<vec.at(i)<<std::endl;
                break;    
            }
            std::cout<<vec.at(i)<<",";
            }
        }   




}
