#include <vector>
#include <string>
class Tokenizer{
    private:
        std::vector< std::vector<std::string> > *data;
        int size;
        
    public:
        Tokenizer();
        ~Tokenizer();
        std::vector<std::string> GetTokens();
        void Tokenize(std::string data);
        bool isInt(std::string data);
        int getSize();
}; 
