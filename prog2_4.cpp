#include "prog2_1.hpp"
#include "prog2_3.hpp"
#include <stdio.h>
#include <vector>
#include <string>
#include <sstream>
#include <stdexcept>
#include <iostream>
#include <fstream>
int main(int argc, char** argv){
    printf("Assignment #2-4, Sergio Santana, rigosanle@gmail.com\n");
    Tokenizer tok;
    std::ifstream file(argv[1]);
    int j=1;
    try{
    while(!file.eof()){
        std::string s;
        getline(file,s);    
        tok.Tokenize(s);   
        j++;
           }
        }   
    catch(std::string e){
    std::cout<<"Error on line "<<j<<": "<< e<< std::endl;
    return 0;
    }

    
    //necessary to print out, because once i use getTokens()
    //for the parser i basically lose the tokens
    std::vector<std::vector<std::string> > array;

    Parser parse;
    int size=tok.getSize();
    
    for(int i=0;i<size;i++){
        std::vector<std::string> temp=tok.GetTokens();
        array.push_back(temp);
        bool parsed=parse.Parse(temp);
        if(!parsed){
             std::cout<<"Parse error on line "<<i+1<<": ";
             for(int m=0;m<temp.size();m++)
                std::cout<<temp.at(m)<<" ";
            return 0;
        }

    }
    
    int size1=array.size();
    
    for(int m=0;m<size1;m++){
        
        std::vector<std::string> vec=array.at(m);
        int vecSize=vec.size();
        for(int i=0;i<vecSize;i++){
            if(i==vecSize-1){ 
                std::cout<<vec.at(i)<<std::endl;
                break;    
            }
            std::cout<<vec.at(i)<<",";
            }
        }   



}





