#include "prog2_1.hpp"
#include <string>
#include <iostream>
#include <vector>

#include <fstream>
int main(int argc,char** argv){
    Tokenizer tok;
    std::ifstream file(argv[1]);
    int j=1;
    try{
    while(!file.eof()){
        std::string s;
        getline(file,s);    
        tok.Tokenize(s);   
        j++;
           }
        }   
    catch(std::string e){
    std::cout<<"Error on line "<<j<<": "<< e<< std::endl;
    }

}
