prog2_1:
Is a class called Tokenizer.
Tokenize(String str):takes in a string and checks that tokens are correct. Adds to vector
GetTokens(): return vector of each line taken from input file


prog2_2:
Is the driver that takes in a file name from command line and creates a Tokenizer object that Tokenizes by line from the file.

prog2_3:
Defines a class called Parser.
Parse(Vector vec): return true if order of tokens is appropriate.

prog2_4:
Driver that takes in a file name from command line and Tokenizes and then Parses each line from the input file.

