#include "prog2_1.hpp"
#include <stdio.h>
#include <vector>
#include <string>
#include <sstream>
#include <stdexcept>
#include <iostream>
Tokenizer::Tokenizer(){
    
    this->size=0;
    this->data=new std::vector<std::vector<std::string> >();
}
Tokenizer::~Tokenizer(){
    delete this->data;
}
std::vector<std::string> Tokenizer::GetTokens(){
    std::vector<std::string> temp=this->data->front();
    this->data->erase(this->data->begin());
    this->size--;
    return temp;
}
void Tokenizer::Tokenize(std::string data){
   
    std::stringstream oss(data);
    std::vector<std::string> temp;
    if(data==" "||data=="") return;
     while(!oss.eof()){
        
        std::string s;
        oss>>s;
        
        
        if(s.compare("push")==0||s.compare("pop")==0||s.compare("add")==0||
        s.compare("sub")==0||s.compare("mul")==0||s.compare("div")==0||
        s.compare("mod")==0||s.compare("skip")==0||s.compare("save")==0||
        s.compare("get")==0||isInt(s)&&s.compare("")!=0 ){
            temp.push_back(s);
            }
        
        else if(s.compare("")==0) ;
        else{
           throw "Unexpected token: "+s;
            
            }
        
        }
    
    this->data->push_back(temp);
    this->size++;
    return;
}
bool Tokenizer::isInt(std::string data){
    const char *temp=data.c_str();
    int i=0;
    while(i<data.length()){
        if(data.at(i)<48||data.at(i)>57)
            return false;
        i++;
    }
    
    return true;   
}
int Tokenizer::getSize(){
    return this->size;
}



